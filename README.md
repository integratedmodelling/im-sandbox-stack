# Klab SandBox for Kubernetes

Included in this git are all of the configurations needed to stand up an Integrated modelling sandbox.

## Current Software 	

1. K.Lab Hub Stack
    * K.Lab Hub Container
    * Mongo Container
    * Ldap Container
2. K.Lab Node Stack
    * K.Lab Node Container
    * PostGIS Container 3.0+
    * Connection to Geoserver
3. Geoserver 2.15.5
4. Ingress Config
5. K.Lab Engine Stack 
    * K.Lab Engine Container
    * HAProxy Load balancer (deal with sticky sessions and engine loads)


Currently itmes 1 through 3 are finished, and work has begun on bring 4 and 5 into the stack.

To spoof a the expected fully qualified domain name for the hub and node, a klab.info entry should be added to the hostfile. Also regcred imagepull secret will be needed in order to pull from the private container registry of integrated modelling.



#### ToDo

1. Need to add email support to the hub sandbox, perhaps greenmail container.

2. Need to add replicas of the engine and test my haproxy config and check if the agent service check also works.

3. One day there will be nice instructions
